# Book Manager Secure

Proyecto con el manejador de libros online de la cuarta práctica
de la asignatura de Tecnologías de Servicios de Internet
del Máster de Cloud Apps de la URJC, parte escrita en [Node.js][1].

El servicio de login es el `POST` de **/users** Cuando se invoca a este servicio, en la
cabecera `Authorization` nos viene devuelto el tojken JWT.

Además se ha implementado un servicio de login (bajo la ruta `/login`) que nos devuelve
de la misma forma el token del usuario.

[1]: https://nodejs.org/es/