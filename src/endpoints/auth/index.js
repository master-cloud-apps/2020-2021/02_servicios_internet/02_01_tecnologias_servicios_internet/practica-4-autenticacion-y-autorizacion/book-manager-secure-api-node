const User = require('./../../models/user.js')

const handlers = () => ({

  login: async (req, res) => {
    try {
      const user = await User.finByCredentials(req.body.email, req.body.password)
      const token = await user.generateAuthToken()
      return res.status(201).header('Authorization', token).send(user)
    } catch (error) {
      return res.status(400).send(error)
    }
  }
})

module.exports = handlers
