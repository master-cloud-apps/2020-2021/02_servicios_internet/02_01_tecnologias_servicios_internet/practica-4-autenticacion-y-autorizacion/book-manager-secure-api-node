const users = require('./users/index.js')
const books = require('./books/index.js')
const comments = require('./comments/index.js')
const auth = require('./auth/index.js')

module.exports = {
  users,
  books,
  comments,
  auth
}
