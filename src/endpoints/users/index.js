const User = require('./../../models/user.js')
const { getFullUrl, checkModelNone, checkMongoId, manageChainError, createModel } = require('./../utils/index.js')
const _ = require('lodash')

const createUserFromRequestBody = (request) => (new User({
  nick: request.body.nick,
  email: request.body.email,
  password: request.body.password
}))

const handlers = () => ({
  get: (req, res) => {
    return User.find()
      .then(users => res.status(200).send(users.map(user => user.toJSON())))
  },
  post: (req, res) => {
    const checkNick = (nick) =>
      User.find({ nick })
        .then(users => {
          if (users.length > 0) {
            return Promise.reject(res.sendStatus(400))
          }
          return Promise.resolve(users)
        })

    return Promise.resolve(checkNick(req.body.nick))
      .then(() => createUserFromRequestBody(req))
      .then(user => createModel(user, res)
        .then(() => user.generateAuthToken())
        .then(token => res.status(201).location(getFullUrl(req) + user.id).header('authorization', token).send()))
      .catch(error => manageChainError(error, res))
  },
  getById: (req, res) => {
    const checkUserNone = user => {
      return checkModelNone(user, res)
    }

    const mapUser = user => ({ id: user._id, nick: user.nick, email: user.email })

    return Promise.resolve(checkMongoId(req.params.userId, res))
      .then(id => User.findById(id))
      .then(user => checkUserNone(user))
      .then(user => mapUser(user))
      .then(mappedUser => res.status(200).send(mappedUser))
      .catch(error => res.status(500).send(error))
  },
  getCommentsByUserId: (req, res) => {
    const mapComment = comment => ({
      book: {
        id: comment.book
      },
      id: comment._id,
      content: comment.content,
      punctuation: comment.punctuation

    })

    return Promise.resolve(checkMongoId(req.params.userId, res))
      .then(() => User.findById(req.params.userId))
      .then(user => checkModelNone(user, res))
      .then(user => user.populate({ path: 'comments' }).execPopulate())
      .then(populatedUser => res.status(200).send(populatedUser.comments.map(mapComment)))
      .catch(error => manageChainError(error, res))
  },
  updateById: (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['email']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    const checkValidOperation = () => {
      if (!isValidOperation) {
        return Promise.reject(res.status(400).send({ error: 'Invalid update field' }))
      }
      return Promise.resolve(isValidOperation)
    }
    const mapUserForUpdate = user => {
      return updates.reduce((userToUpdate, update) => {
        userToUpdate[update] = req.body[update]
        return userToUpdate
      }, _.cloneDeep(user))
    }

    const includeIdToModel = model => {
      const idModel = _.cloneDeep(model)
      idModel._id = req.params.userId
      idModel.isNew = false
      return idModel
    }

    return checkValidOperation()
      .then(() => User.findById(req.params.userId))
      .then(user => checkModelNone(user, res))
      .then(user => includeIdToModel(user))
      .then(user => mapUserForUpdate(user))
      .then(user => createModel(user, res))
      .then(user => res.status(200).send(user.toJSON()))
      .catch(error => res.status(500).send(error))
  },
  deleteById: (req, res) => {
    const checkCommentsNone = (user) => {
      return user.populate({ path: 'comments' }).execPopulate()
        .then(populatedUser => {
          if (populatedUser.comments.length > 0) {
            return Promise.reject(res.status(400).send({ error: 'User with comments' }))
          }
          return populatedUser
        })
    }

    return Promise.resolve(checkMongoId(req.params.userId, res))
      .then(id => User.findById(id))
      .then(user => checkModelNone(user, res))
      .then(user => checkCommentsNone(user))
      .then(user => User.findOneAndDelete({ _id: user._id }))
      .then(user => res.status(204).send(user.toJSON()))
      .catch(error => manageChainError(error, res))
  }
})

module.exports = handlers
