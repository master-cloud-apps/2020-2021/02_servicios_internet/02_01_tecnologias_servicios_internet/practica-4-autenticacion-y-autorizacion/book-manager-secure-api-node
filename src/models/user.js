const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
  nick: { type: String, required: true, unique: true, dropDups: true },
  email: { type: String, required: true, unique: true },
  password: {
    type: String,
    required: true,
    minlength: 7,
    trim: true,
    validate (value) {
      if (value.toLowerCase().includes('password')) {
        throw new Error('Password cannot contain "password"')
      }
    }
  },
  tokens: [{
    token: {
      type: String,
      required: true
    }
  }]
})

userSchema.virtual('comments', {
  ref: 'Comment',
  localField: '_id',
  foreignField: 'user'
})

userSchema.methods.toJSON = function () {
  const userObject = this.toObject()

  userObject.id = userObject._id

  delete userObject._id
  delete userObject.__v
  delete userObject.password
  delete userObject.tokens

  return userObject
}

userSchema.methods.generateAuthToken = function () {
  const user = this
  const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET)

  user.tokens = user.tokens.concat({ token })
  return user.save().then(() => token)
}

const User = mongoose.model('Users', userSchema)

userSchema.statics.finByCredentials = function (email, password) {
  return User.findOne({ email })
    .then(user => (user ? bcrypt.compare(password, user.password) : Promise.reject(new Error({ errorMessage: 'User not found' })))
      .then(areTheSame => areTheSame ? user : Promise.reject(new Error({ errorMessage: 'Not the same password' }))))
}

userSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    return bcrypt.genSalt(10)
      .then(salt => bcrypt.hash(this.password, salt))
      .then(hash => (this.password = hash))
      .then(() => next())
  } else {
    next()
  }
})

module.exports = mongoose.model('User', userSchema)
