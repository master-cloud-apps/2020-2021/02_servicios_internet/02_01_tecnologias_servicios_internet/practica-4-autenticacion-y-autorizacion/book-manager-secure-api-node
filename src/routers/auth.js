const express = require('express')
const { auth } = require('../endpoints/index.js')

const router = new express.Router()
const authHandler = auth()

router.post('/login', authHandler.login)
module.exports = router
