const express = require('express')
const { comments } = require('./../endpoints/index.js')
const { verify } = require('../middlewares/auth/index.js')

const router = new express.Router()
const commentHandler = comments()

router.post('/books/:bookId/comments', verify, commentHandler.post)
router.get('/books/:bookId/comments/:commentId', verify, commentHandler.getById)

module.exports = router
