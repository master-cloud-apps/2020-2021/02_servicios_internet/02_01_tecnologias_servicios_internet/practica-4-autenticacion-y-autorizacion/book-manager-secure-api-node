const user = require('./user.js')
const book = require('./book.js')
const comment = require('./comment.js')
const auth = require('./auth.js')

module.exports = {
  userRouter: user,
  bookRouter: book,
  commentRouter: comment,
  authRouter: auth
}
