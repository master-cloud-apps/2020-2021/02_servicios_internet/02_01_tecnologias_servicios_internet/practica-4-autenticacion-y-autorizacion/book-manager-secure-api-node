const express = require('express')
const { users } = require('./../endpoints/index.js')
const { verify } = require('../middlewares/auth/index.js')

const router = new express.Router()
const userHandler = users()

router.post('/users', userHandler.post)
router.get('/users', verify, userHandler.get)
router.get('/users/:userId/comments', verify, userHandler.getCommentsByUserId)
router.get('/users/:userId', verify, userHandler.getById)
router.put('/users/:userId', verify, userHandler.updateById)
router.delete('/users/:userId', verify, userHandler.deleteById)

module.exports = router
