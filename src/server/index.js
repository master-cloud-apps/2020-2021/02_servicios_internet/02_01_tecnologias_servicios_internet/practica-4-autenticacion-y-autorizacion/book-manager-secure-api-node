const https = require('https')
const fs = require('fs')
const app = require('./../app/index.js')

const createServer = () => https.createServer({
  key: fs.readFileSync(process.env.SERVER_KEY),
  cert: fs.readFileSync(process.env.SERVER_CERT)
}, app)

module.exports = createServer
