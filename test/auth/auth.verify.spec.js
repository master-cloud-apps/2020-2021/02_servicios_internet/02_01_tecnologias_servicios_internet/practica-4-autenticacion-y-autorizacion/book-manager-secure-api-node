const { expect } = require('chai')
const jwt = require('jsonwebtoken')
const sinon = require('sinon')
const { verify } = require('../../src/middlewares/auth/index.js')
const { constructRequestForTest, constructResponseForTest } = require('./../endpoints/index.js')

describe('Verify handler use cases', () => {
  let res
  beforeEach(() => {
    res = constructResponseForTest()
  })
  it('When no jwt token sent, should return 401 response', () => {
    const spyNext = sinon.spy()
    const req = constructRequestForTest()

    const resVerify = verify(req, res, spyNext)
    expect(resVerify.statusCode).to.be.equal(401)
    expect(resVerify.body.error).to.be.equal('Bad authentication')
    expect(spyNext.getCalls().length).to.be.equal(0)
  })
  it('When jwt not valid token sent, should return 401 response', () => {
    const spyNext = sinon.spy()
    const req = constructRequestForTest()

    const token = '12345'
    req.setHeader('Authorization', `Bearer ${token}`)
    const resVerify = verify(req, res, spyNext)
    expect(resVerify.statusCode).to.be.equal(401)
    expect(resVerify.body.error).to.be.equal('Bad authentication')
    expect(spyNext.getCalls().length).to.be.equal(0)
  })
  it('When jwt valid token sent, should return token in request', () => {
    const spyNext = sinon.spy()
    const req = (() => {
      const headers = {}
      return {
        header: headerName => headers[headerName],
        setHeader: (headerName, headerValue) => (headers[headerName] = headerValue)
      }
    })()

    const token = jwt.sign({ _id: 12345 }, process.env.JWT_SECRET)
    req.setHeader('Authorization', `Bearer ${token}`)
    verify(req, res, spyNext)
    expect(req.token).to.be.equal(token)
    expect(spyNext.getCalls().length).to.be.equal(1)
  })
})
