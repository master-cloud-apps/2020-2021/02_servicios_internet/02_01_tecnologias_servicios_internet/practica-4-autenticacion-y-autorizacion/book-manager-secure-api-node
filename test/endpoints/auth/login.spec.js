const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { expect } = require('chai')
const { manageInMemoryDatabase } = require('./../index.js')
const { setupDatabase, userOne } = require('../../fixtures/db.js')

describe('Login use cases', () => {
  manageInMemoryDatabase()
  beforeEach(setupDatabase)

  describe('Correct logins over user already populated', () => {
    it('When login correct email and pass should return token', () => {
      return request(app).post('/login').send({
        email: userOne.email,
        password: userOne.password
      })
        .then(response => {
          expect(response.statusCode).to.be.equal(201)
          expect(response.headers.authorization).to.not.be.equal(undefined)
        })
    })
    it('Given wrong email When login should return 400', () => {
      return request(app).post('/login').send({
        email: 'other',
        password: userOne.password
      })
        .then(response => {
          expect(response.statusCode).to.be.equal(400)
        })
    })
    it('Given wrong pass When login should return 400', () => {
      return request(app).post('/login').send({
        email: userOne.email,
        password: 'worn_pass'
      })
        .then(response => {
          expect(response.statusCode).to.be.equal(400)
        })
    })
  })
})
