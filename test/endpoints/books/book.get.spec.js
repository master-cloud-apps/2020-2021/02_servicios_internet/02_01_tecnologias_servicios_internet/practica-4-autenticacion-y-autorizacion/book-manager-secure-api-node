const request = require('supertest')
const app = require('./../../../src/app/index.js')
const { getIdFromResponseLocationHeader, manageInMemoryDatabase } = require('./../index.js')
const { expect } = require('chai')
const { bookTest, deleteBookById, getBookById, createTestBook } = require('../book.js')
const { createTestUserAndGetId, getUserById } = require('./../user.js')
const { createTestComment, getCommentTest } = require('./../comment.js')
const { setupDatabase, userOne } = require('../../fixtures/db.js')

describe('Book use cases', () => {
  manageInMemoryDatabase()
  beforeEach(setupDatabase)

  const token = userOne.tokens[0].token
  const createBookSecure = () => createTestBook(app, token)

  describe('GET /books/:bookId use cases', () => {
    let bookTestId
    beforeEach(() => createBookSecure(app, token)
      .then(response => getIdFromResponseLocationHeader(response))
      .then(bookId => (bookTestId = bookId)))
    it('Given book created, should return the book by id', () => {
      return request(app)
        .get(`/books/${bookTestId}`)
        .set('Authorization', `Bearer ${token}`)
        .then(response => {
          expect(response.statusCode).to.equal(200)
          expect(response.body).to.deep.include(bookTest)
        })
    })
    it('When get book by id wrong should return bad request', () => {
      return request(app)
        .get('/books/12345')
        .set('Authorization', `Bearer ${token}`)
        .then(response => expect(response.status).to.be.equal(400))
    })
    it('Given created book when delete book then get book by id should return 404', () => {
      return deleteBookById({ bookId: bookTestId, app, token })
        .then(response => expect(response.statusCode).to.be.equal(204))
        .then(_ => getBookById({ bookId: bookTestId, app, token }))
        .then(response => expect(response.statusCode).to.be.equal(404))
    })
  })
  describe('GET /books use cases', () => {
    it('Given no book created, should return ok', () => {
      return request(app).get('/books')
        .then(response => expect(response.statusCode).to.be.equal(200))
    })
    it('Given no book created, should return empty list', () => {
      return request(app).get('/books')
        .then(response => expect(response.body).to.be.deep.equal([]))
    })
    it('Given two books created, should return two elements', () => {
      return Promise.all([createBookSecure(app), createBookSecure(app)])
        .then(responses => request(app).get('/books'))
        .then(response => expect(response.body.length).to.be.equal(2))
    })
    it('Given three books created, should return three elements', () => {
      return Promise.all([createBookSecure(app), createBookSecure(app), createBookSecure(app)])
        .then(() => request(app)
          .get('/books')
          .set('Authorization', `Bearer ${token}`))
        .then(response => {
          expect(response.body.length).to.be.equal(3)
          expect(response.body[2]).to.deep.include(bookTest)
        })
    })
    describe('GET /books with comments', () => {
      let testBookId
      let user
      beforeEach(() => createBookSecure(app)
        .then(response => getIdFromResponseLocationHeader(response))
        .then(bookId => (testBookId = bookId))
        .then(() => createTestUserAndGetId(app))
        .then(userId => getUserById({ userId, app, token }))
        .then(response => (user = response.body))
        .then(() => createTestComment({ app, bookId: testBookId, nick: user.nick, token })))
      it('Given book with one comment, when call get books, should return the one comment', () => {
        return request(app).get('/books').set('Authorization', `Bearer ${token}`)
          .then(response => {
            expect(response.statusCode).to.be.equal(200)
            expect(response.body.length).to.be.equal(1)
            expect(response.body[0]).to.deep.include(bookTest)
            expect(response.body[0].comments.length).to.be.equal(1)
            expect(response.body[0].comments[0].content).to.be.equal(getCommentTest(user.nick).content)
            expect(response.body[0].comments[0].punctuation).to.be.equal(getCommentTest(user.nick).punctuation)
          })
      })
    })
    describe('GET /books user not registered rol', () => {
      it('Given three books created, should return three elements wirth only public properties', () => {
        return Promise.all([createBookSecure(app), createBookSecure(app), createBookSecure(app)])
          .then(() => request(app).get('/books'))
          .then(response => {
            expect(response.body.length).to.be.equal(3)
            expect(response.body[2]).to.deep.include({
              title: bookTest.title
            })
            expect(response.body[2].yearPublication).to.be.equal(undefined)
          })
      })
    })
  })
})
