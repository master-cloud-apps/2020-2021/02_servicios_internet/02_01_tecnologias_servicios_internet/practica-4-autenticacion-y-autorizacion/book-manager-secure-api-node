const dbHandler = require('./../db-handler.js')
const request = require('supertest')

const getIdFromLocation = location =>
  location.split('/')[location.split('/').length - 1]

const getIdFromResponseLocationHeader = response =>
  getIdFromLocation(response.headers.location)

const manageInMemoryDatabase = () => {
  before(() => dbHandler.connect())

  afterEach(() => dbHandler.clearDatabase())

  after(() => dbHandler.closeDatabase())
}

const getUrlByModel = ({ modelType, bookId = undefined }) => {
  const urlByModel = {
    book: '/books',
    comment: `/books/${bookId}/comments`,
    user: '/users'
  }
  return urlByModel[modelType]
}

const createModel = ({ model, modelType, bookId = undefined, app, token }) =>
  request(app).post(getUrlByModel({ modelType, bookId })).set('Authorization', `Bearer ${token}`).send(model)

const constructResponseForTest = function () {
  return {
    status: function (code) {
      this.statusCode = code
      return this
    },
    send: function (data) {
      this.body = data
      return this
    },
    statusCode: this.statusCode,
    body: this.body
  }
}

const constructRequestForTest = () => {
  const headers = {}
  return {
    header: headerName => headers[headerName],
    setHeader: (headerName, headerValue) => (headers[headerName] = headerValue)
  }
}

module.exports = {
  getIdFromResponseLocationHeader, manageInMemoryDatabase, createModel, constructResponseForTest, constructRequestForTest
}
