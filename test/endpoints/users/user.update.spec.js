const app = require('./../../../src/app/index.js')
const { manageInMemoryDatabase } = require('./../index.js')
const { testUser, createTestUserAndGetId, updateUserById, getUserById } = require('./../user.js')
const { setupDatabase, userOne } = require('../../fixtures/db.js')
const { expect } = require('chai')
const _ = require('lodash')

describe('User use cases', () => {
  manageInMemoryDatabase()
  beforeEach(setupDatabase)

  const token = userOne.tokens[0].token
  describe('PUT /users/:userId use cases', () => {
    it('Given user created, when update user email should return ok', async () => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.email = 'updated_email@test.com'
      delete userToUpdate.nick
      delete userToUpdate.password
      const userId = await createTestUserAndGetId(app)

      const responsePut = await updateUserById({ app, token, user: userToUpdate, userId })

      expect(responsePut.statusCode).to.be.equal(200)
    })
    it('Given user created, when update user email should return user with email modified', async () => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.email = 'updated_email@test.com'
      delete userToUpdate.nick
      delete userToUpdate.password
      const userId = await createTestUserAndGetId(app)

      const responsePut = await updateUserById({ app, token, user: userToUpdate, userId })

      expect(responsePut.body.email).to.be.equal('updated_email@test.com')
    })
    it('Given user updated when get user by id should return user with email modified', async () => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.email = 'updated_email@test.com'
      delete userToUpdate.nick
      delete userToUpdate.password
      const userId = await createTestUserAndGetId(app)
      await updateUserById({ app, token, user: userToUpdate, userId })

      const responseGet = await getUserById({ app, token, userId })

      expect(responseGet.body.email).to.be.equal('updated_email@test.com')
    })
    it('Given user created, when update user nick should return bad request', async () => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.nick = 'updated_nick'
      const userId = await createTestUserAndGetId(app)

      const response = await updateUserById({ app, token, user: userToUpdate, userId })

      expect(response.statusCode).to.be.equal(400)
    })
    it('when update user with not found id should return not found', async () => {
      const userToUpdate = _.cloneDeep(testUser)
      userToUpdate.nick = 'updated_nick'
      const userId = await createTestUserAndGetId(app)

      const response = await updateUserById({ app, token, user: userToUpdate, userId })

      expect(response.statusCode).to.be.equal(400)
    })
  })
})
