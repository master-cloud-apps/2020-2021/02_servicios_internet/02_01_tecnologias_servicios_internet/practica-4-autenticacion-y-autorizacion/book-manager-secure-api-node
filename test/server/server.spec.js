const { expect } = require('chai')
const chai = require('chai')
const chaiHttp = require('chai-http')
const createServer = require('./../../src/server/index.js')
const { manageInMemoryDatabase } = require('./../endpoints/index.js')

chai.use(chaiHttp)

describe('Create server tests', () => {
  manageInMemoryDatabase()
  it('Given secure server, should require https to connect', (done) => {
    const server = createServer()
    return server.listen(3001, () => {
      return chai.request('https://localhost:3001')
        .post('/users')
        .send({ nick: null })
        .then(response => {
          expect(response.body.error.nick.kind).to.be.equal('required')
          expect(response).to.have.status(400)
          server.close()
          done()
        }).catch(done)
    })
  })
})
